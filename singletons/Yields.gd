extends Node


func TimeOrInputPressed(duration: float, action: String) -> void:
	var endTime: float = OS.get_ticks_msec() + duration * 1000
	yield(get_tree(), "idle_frame")
	while OS.get_ticks_msec() < endTime:
		if Input.is_action_just_pressed(action):
			break
		yield(get_tree(), "idle_frame")


func InputPressed(action: String) -> void:
	yield(get_tree(), "idle_frame")
	while ! Input.is_action_just_pressed(action):
		yield(get_tree(), "idle_frame")


func _ready() -> void:
	self.pause_mode = Node.PAUSE_MODE_PROCESS
