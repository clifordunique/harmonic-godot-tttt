extends Node

var _data: Dictionary = {}


func Get(key: String, default):
	return _data.get(key, default)


func Set(key: String, value):
	_data[key] = value
	_save()


func _ready() -> void:
	var file = File.new()
	if file.open("user://savegame.txt", File.READ) == OK:
		_data = parse_json(file.get_as_text()) as Dictionary
		file.close()


func _save() -> void:
	var file = File.new()
	file.open("user://savegame.txt", File.WRITE)
	file.store_line(to_json(_data))
	file.close()
