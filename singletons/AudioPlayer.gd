# Usage:
#
# const music = preload("res://music/music.ogg")
# AudioPlayer.PlayMusic(music)
# yield(AudioPlayer.FadeMusic(music, 3), "complete")
#
# const sfx = preload("res://sfx/sfx.wav")
# yield(AudioPlayer.playSound(sfx, 1), "complete")

extends Node

const SFX_STREAMS: int = 32

var _musicPlayer: AudioStreamPlayer = AudioStreamPlayer.new()
var _musicTween: Tween = Tween.new()
var _sfxPlayers: Array = []
var _playedSounds: Dictionary


func PlayMusic(stream: AudioStream, bus: String = "Music") -> void:
	_musicTween.remove_all()
	_musicPlayer.stream = stream
	_musicPlayer.bus = bus
	_musicPlayer.volume_db = 0
	_musicPlayer.stream_paused = false
	_musicPlayer.play()


func StopMusic() -> void:
	_musicPlayer.stop()


func FadeMusic(duration: float, targetVolume: float = 0, stopAtEnd: bool = true) -> void:
	_musicTween.remove_all()
	_musicTween.interpolate_method(
		self,
		"_onTweenUpdate",
		AudioUtils.DbToVolume(_musicPlayer.volume_db),
		targetVolume,
		duration,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN
	)
	_musicTween.start()
	yield(_musicTween, "tween_completed")
	if stopAtEnd:
		_musicPlayer.stop()


func PlaySound(stream: AudioStream, volume: float = 1, pitch_scale: float = 1, bus: String = "SFX") -> void:
	if _playedSounds.has(stream):
		return
	_playedSounds[stream] = true
	for sfxPlayer in _sfxPlayers:
		if ! sfxPlayer.playing:
			sfxPlayer.stream = stream
			sfxPlayer.bus = bus
			sfxPlayer.volume_db = AudioUtils.VolumeToDb(volume)
			sfxPlayer.pitch_scale = pitch_scale
			sfxPlayer.play()
			yield(sfxPlayer, "finished")
			return


func _ready() -> void:
	self.pause_mode = Node.PAUSE_MODE_PROCESS
	_musicPlayer.name = "MusicPlayer"
	_musicTween.name = "MusicTween"
	add_child(_musicPlayer)
	add_child(_musicTween)
	for i in range(SFX_STREAMS):
		_sfxPlayers.append(AudioStreamPlayer.new())
		_sfxPlayers[i].name = "SfxPlayer %s" % i
		add_child(_sfxPlayers[i])


func _process(_delta: float) -> void:
	_playedSounds.clear()


func _onTweenUpdate(volume: float) -> void:
	_musicPlayer.volume_db = AudioUtils.VolumeToDb(volume)
