extends Node

var _currentDirection: int = -1


func _process(_delta: float) -> void:
	_currentDirection = Direction4FromInput()


func Direction4FromInput() -> int:
	if LeftJustPressed():
		return Direction4.LEFT
	if RightJustPressed():
		return Direction4.RIGHT
	if UpJustPressed():
		return Direction4.UP
	if DownJustPressed():
		return Direction4.DOWN
	if LeftPressed() && ! Direction4.IsVertical(_currentDirection):
		return Direction4.LEFT
	if RightPressed() && ! Direction4.IsVertical(_currentDirection):
		return Direction4.RIGHT
	if UpPressed() && ! Direction4.IsHorizontal(_currentDirection):
		return Direction4.UP
	if DownPressed() && ! Direction4.IsHorizontal(_currentDirection):
		return Direction4.DOWN

	return -1


func UpPressed() -> bool:
	return Input.is_action_pressed("up") && ! Input.is_action_pressed("down")


func UpJustPressed() -> bool:
	return Input.is_action_just_pressed("up") && ! Input.is_action_pressed("down")


func DownPressed() -> bool:
	return Input.is_action_pressed("down") && ! Input.is_action_pressed("up")


func DownJustPressed() -> bool:
	return Input.is_action_just_pressed("down") && ! Input.is_action_pressed("up")


func LeftPressed() -> bool:
	return Input.is_action_pressed("left") && ! Input.is_action_pressed("right")


func LeftJustPressed() -> bool:
	return Input.is_action_just_pressed("left") && ! Input.is_action_pressed("right")


func RightPressed() -> bool:
	return Input.is_action_pressed("right") && ! Input.is_action_pressed("left")


func RightJustPressed() -> bool:
	return Input.is_action_just_pressed("right") && ! Input.is_action_pressed("left")


func VerticalPressed() -> bool:
	return UpPressed() || DownPressed()


func HorizontalPressed() -> bool:
	return LeftPressed() || RightPressed()
