class_name Direction4

enum { LEFT = 0, DOWN = 1, UP = 2, RIGHT = 3 }

static func ToVector2(direction: int) -> Vector2:
	match direction:
		LEFT:
			return Vector2.LEFT
		DOWN:
			return Vector2.DOWN
		UP:
			return Vector2.UP
		RIGHT:
			return Vector2.RIGHT
	return Vector2.ZERO

static func Opposite(direction: int) -> int:
	match direction:
		LEFT:
			return RIGHT
		DOWN:
			return UP
		UP:
			return DOWN
		RIGHT:
			return LEFT
	return -1

static func RotateCW(direction: int) -> int:
	match direction:
		LEFT:
			return UP
		DOWN:
			return LEFT
		UP:
			return RIGHT
		RIGHT:
			return DOWN
	return -1

static func RotateCCW(direction: int) -> int:
	match direction:
		LEFT:
			return DOWN
		DOWN:
			return RIGHT
		UP:
			return LEFT
		RIGHT:
			return UP
	return -1

static func IsHorizontal(direction: int) -> bool:
	return direction == LEFT || direction == RIGHT

static func IsVertical(direction: int) -> bool:
	return direction == UP || direction == DOWN
