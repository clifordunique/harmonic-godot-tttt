class_name AudioUtils

static func VolumeToDb(volume: float) -> float:
	if volume == 0:
		return -INF
	return 20 * log(volume)

static func DbToVolume(db: float) -> float:
	return pow(10, db / 20)

static func SetLoop(sound: AudioStream, loop: bool) -> void:
	var oggStream: AudioStreamOGGVorbis = sound as AudioStreamOGGVorbis
	if oggStream:
		oggStream.loop = loop
		return

	var sampleStream: AudioStreamSample = sound as AudioStreamSample
	if sampleStream:
		sampleStream.loop_mode = (
			AudioStreamSample.LoopType.LOOP_FORWARD
			if loop
			else AudioStreamSample.LoopType.LOOP_DISABLED
		)
		return

	var mp3Stream: AudioStreamMP3 = sound as AudioStreamMP3
	if mp3Stream:
		mp3Stream.loop = loop
		return

	push_error("Invalid AudioStream type for SetLoop: %s" % sound.get_class())