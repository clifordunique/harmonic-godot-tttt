shader_type canvas_item;

uniform float blend_amount = 0.5;

uniform vec4 color : hint_color = vec4(1, 1, 1, 1);

void fragment(){
	COLOR = texture(TEXTURE, UV);
  COLOR.rgb = COLOR.rgb * (1.0 - blend_amount) + color.rgb * blend_amount;
}
