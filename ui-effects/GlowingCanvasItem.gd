# GlowingCanvasItem
# Attach to any CanvasItem to make it transition between its initial color and a glow color.

extends CanvasItem

export var Period: float = 1
export var GlowColor: Color = Color.transparent
export var Enabled: bool = true

# When set to false, this doesn't affect pressed and disabled buttons.
export var AffectPressedDisabled: bool = false

var _initialColor: Color


func _ready() -> void:
	_initialColor = modulate


func _process(_delta: float) -> void:
	if ! _shouldProcess():
		modulate = _initialColor
		return
	var l: float = sin(OS.get_ticks_msec() * .001 * 2 * PI / Period) * 0.5 + 0.5
	modulate = lerp(_initialColor, GlowColor, l)


func _shouldProcess() -> bool:
	if ! Enabled:
		return false
	var button: Button = get_node('.') as Button
	if button != null && (button.pressed || button.disabled):
		return false
	return true
